#!/usr/bin/env python3

from setuptools import setup

setup(name='DNS-LG',
      version='2020010301', 
      description='DNS Looking Glass',
      license='BSD',
      author='Stephane Bortzmeyer',
      author_email='bortzmeyer+dnslg@nic.fr',
      url='https://framagit.org/bortzmeyer/dns-lg',
      download_url='https://framagit.org/bortzmeyer/dns-lg/-/archive/master/dns-lg-master.tar',
      packages=['DNSLG',],
      provides=['DNSLG',],
      install_requires=[] # We require netaddr, simpletal, webob and
                          # dnspython but Python cannot find them,
                          # even when installed :-( Packaging in
                          # Python is completely broken, anyway
      )

