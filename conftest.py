import pytest

def pytest_addoption(parser):
    parser.addoption("--url", action="store", default='http://localhost:8080/',
                     help="URL of the server to test")

@pytest.fixture
def prefix(request):
    return request.config.getoption("--url")
